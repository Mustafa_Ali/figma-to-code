const searchBox = document.querySelector(".search-box");
const iconImg = document.querySelector(".icon-img");
const searchIcon = document.querySelector(".search-icon");
const closeIcon = document.querySelector(".close-icon");

searchIcon.addEventListener("click", () => {
  searchBox.classList.add("active");
  iconImg.classList.add("active");
});

closeIcon.addEventListener("click", () => {
  searchBox.classList.remove("active");
  iconImg.classList.remove("active");
});
